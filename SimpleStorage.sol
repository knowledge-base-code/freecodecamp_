// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;

contract SimpleStorage {
    uint256 favoriteNumber = 0;

    struct People {
        string name;
        uint256 favoriteNumber;
    }

    People[] public peoples;
    mapping(string => uint256) public nameToFavoriteNumber;

    function store(uint256 newFavNumber) public {
        favoriteNumber = newFavNumber;
    }

    function viewFavNumber() public view returns (uint256) {
        return favoriteNumber;
    }

    function addPerson(string memory name, uint256 favNumber) public {
        peoples.push(People(name, favNumber));
        nameToFavoriteNumber[name] = favNumber;
    }
}
